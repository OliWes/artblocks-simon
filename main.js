/*
As soon as rendered by ArtBlocks:

const hash = tokenData.hash
const projectNumber = Math.floor(parseInt(tokenData.tokenId) / 1000000)
const mintNumber = parseInt(tokenData.tokenId) % 1000000
*/

// for testing:

function genTokenData(projectNum) {
  let data = {};
  let hash = "0x";
  for (var i = 0; i < 64; i++) {
    hash += Math.floor(Math.random() * 16).toString(16);
  }
  data.hash = hash;
  data.tokenId = (projectNum * 1000000 + Math.floor(Math.random() * 1000)).toString();
  return data;
}
let tokenData = genTokenData(12345);

/* fixed Token
tokenData = {
  hash: "0x11ac16678959949c12d5410212301960fc496813cbc3495bf77aeed738579738", 
  tokenId: "123000456"
}
*/

/**
 * Calculate features for the given token data.
 * @param {Object} tokenData
 * @param {string} tokenData.tokenId - Unique identifier of the token on its contract.
 * @param {string} tokenData.hash - Unique hash generated upon minting the token.
 */
 function calculateFeatures(tokenData) {
  let seed = parseInt(tokenData.hash.slice(0, 16), 16)
  let p = []
  for (let i = 0; i < 20; i++) {
    p.push(tokenData.hash.slice(i+2))
  }
  let rns = p.map(x => {return Math.floor(parseInt(x, 16)) % 15})
  const c = ['red', 'blue', 'yellow'];
  const C = {}
  for (i = 0; i < 20; i++) {
    rns[i]<3 ? C[i] = c[rns[i]] : C[i] = 'white';
  }
  return C
}

C = calculateFeatures(tokenData);
const b = document.querySelectorAll("i :not(.container)");
for (i = 0; i < b.length; i++) {
  b[i].style.backgroundColor = C[i];
}

const colors = ['white','red','blue','yellow']

function changeColor(item) {
  const elem = document.getElementsByClassName(item);
  const currColor = elem[0].style.backgroundColor;
  elem[0].style.backgroundColor = colors[(colors.indexOf(currColor) + 1)%4];
}


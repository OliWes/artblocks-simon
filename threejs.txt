//Artblocks Simon, before random sizes of blocks

import * as THREE from "https://threejs.org/build/three.module.js";
import { OrbitControls } from "https://threejs.org/examples/jsm/controls/OrbitControls.js";
import {mergeVertices} from "https://threejs.org/examples/jsm/utils/BufferGeometryUtils.js";

var camera, scene, renderer, mesh, material, controls, color, geometry, z, stats, light1;
const mouse = new THREE.Vector2( 1, 1 );
const raycaster = new THREE.Raycaster();
//let theta = 0;
//const radius = 360;

const blockMinSize = 20;
const blockDistance = 10;
const blockMinN = 10;
const blockMaxN = 20;
const blockMinZ = 10;
const blockZMultiple = THREE.MathUtils.randInt(10,30);
const blockN = THREE.MathUtils.randInt(blockMinN,blockMaxN);

init();
animate();
getSplit();
addCubes();
render();

function getSplit(){

}

function addCubes() {
		for(var i = 0; i < blockN; i++){
        for(var j = 0; j < blockN; j++){
        		z = THREE.MathUtils.randInt(1,blockZMultiple)*blockMinZ;
        		geometry = new THREE.BoxGeometry(blockMinSize,blockMinSize,z);
            /*geometry.deleteAttribute('normal');
            geometry.deleteAttribute('uv');
            geometry = mergeVertices(geometry);
            geometry.computeVertexNormals();*/
            setColor();
            material = new THREE.MeshLambertMaterial({color:color});
            mesh  = new THREE.Mesh(geometry, material);
        		mesh.position.x = - (blockN-1)/2*(blockMinSize + blockDistance) + ((blockMinSize + blockDistance) * i);
            mesh.position.y = - (blockN-1)/2*(blockMinSize + blockDistance) + ((blockMinSize + blockDistance) * j);
            mesh.position.z = -z/2;
            scene.add(mesh);
        }
    }
}

function setColor() {
	const colors = ['red','blue','yellow'];
  const randNum = THREE.MathUtils.randFloat(0,1);
  if (randNum <= 0.8) {
  	color = 'white';
  } else {
  	color = colors[THREE.MathUtils.randInt(0, 2)]
  }
}

function init() {
    // Renderer.
    renderer = new THREE.WebGLRenderer();
    //renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    // Add renderer to page
    document.body.appendChild(renderer.domElement);

    // Create camera.
    camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight,1, 10000);
    camera.position.z = 500;
    
    // Add controls
    controls = new OrbitControls(camera, renderer.domElement);
    controls.addEventListener('change',render);

    // Create scene.
    scene = new THREE.Scene();
    
    light1 = new THREE.HemisphereLight( 0xffffff, 0x000088 );
    light1.position.set( - 1, 1.5, 1 );
    scene.add( light1 );
    
    

    // Add listener for window resize.
    window.addEventListener('resize', onWindowResize, false);
    document.addEventListener('mousemove',onMouseMove);
}

function animate() {
    controls.update();
    renderer.render( scene, camera );
    requestAnimationFrame(animate);
}

function render() {
		/*
		theta += 0.1;

    camera.position.x = radius * Math.sin( THREE.MathUtils.degToRad( theta ) );
    camera.position.y = radius * Math.sin( THREE.MathUtils.degToRad( theta ) );
    camera.position.z = radius * Math.cos( THREE.MathUtils.degToRad( theta ) );
    camera.lookAt( scene.position );

    camera.updateMatrixWorld();*/
    
    const intersection = raycaster.intersectObject( mesh );
    if ( intersection.length > 0 ) {
      const instanceId = intersection[ 0 ].instanceId;
      mesh.setColorAt( instanceId, color.setHex( Math.random() * 0xffffff ) );
      mesh.instanceColor.needsUpdate = true;
    }
		renderer.render(scene, camera);
    
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
    controls.update();
}

function onMouseMove(event) {
		event.preventDefault();

    mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
    mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
}
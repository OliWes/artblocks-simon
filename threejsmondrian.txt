//Artblocks Simon, piet mondrian original

import * as THREE from "https://threejs.org/build/three.module.js";
import { OrbitControls } from "https://threejs.org/examples/jsm/controls/OrbitControls.js";
import {mergeVertices} from "https://threejs.org/examples/jsm/utils/BufferGeometryUtils.js";

var camera, scene, renderer, mesh, material, controls, color, geometry, z, stats, light1;
const mouse = new THREE.Vector2( 1, 1 );
const raycaster = new THREE.Raycaster();
//let theta = 0;
//const radius = 360;

const blockMinSize = 20;
const blockDistance = 10;
const blockMinN = 10;
const blockMaxN = 20;
const blockMinZ = 10;
const blockZMultiple = THREE.MathUtils.randInt(10,30);
//const blockN = THREE.MathUtils.randInt(blockMinN,blockMaxN);
//mondrian:
const blockN = 34;
var grid = [];
var colorMap = [];

init();
animate();
getSplit();
addCubes();
render();

function getSplit(){
		let i, j;
		for (i = 0; i < blockN; i++) {
      grid[i] = [];
      for (j = 0; j < blockN; j++) {
        grid[i][j] = 0;
      }
    }
    
    //original mondrian
    colorMap = ['red','white','blue','white','white','white','white','white','white','red','white','white','white','white','white','white','white','white','white','yellow','white','white','white','white','red','white','white','white','white','white','white','white','white','white','white','white','white','white','blue'];
    for (i = 0; i < blockN; i++) {
      for (j = 0; j < blockN; j++) {
      	if(i<3){
        	if(j<3){
            grid[i][j] = 1;
          } else if (j<11){
          	grid[i][j] = 2;
          } else if (j<15){
          	grid[i][j] = 3;
          } else if (j<23){
          	grid[i][j] = 4;
          } else if (j<24){
          	grid[i][j] = 5;
          } else if (j<26){
          	grid[i][j] = 6;
          } else if (j<29){
          	grid[i][j] = 7;
          } else if (j<33){
          	grid[i][j] = 8;
          } else{
          	grid[i][j] = 9;
          }
        } else if (i<13){
        	if(j<3){
            grid[i][j] = 10;
          } else if (j<11){
          	grid[i][j] = 11;
          } else if (j<15){
          	grid[i][j] = 12;
          } else if (j<23){
          	grid[i][j] = 13;
          } else if (j<24){
          	grid[i][j] = 14;
          } else if (j<26){
          	grid[i][j] = 15;
          } else if (j<29){
          	grid[i][j] = 16;
          } else if (j<33){
          	grid[i][j] = 17;
          } else{
          	grid[i][j] = 9;
          }
        } else if (i<21){
        	if(j<3){
            grid[i][j] = 10;
          } else if (j<11){
          	grid[i][j] = 18;
          } else if (j<15){
          	grid[i][j] = 19;
          } else if (j<23){
          	grid[i][j] = 20;
          } else if (j<24){
          	grid[i][j] = 21;
          } else if (j<26){
          	grid[i][j] = 22;
          } else if (j<29){
          	grid[i][j] = 23;
          } else if (j<33){
          	grid[i][j] = 24;
          } else{
          	grid[i][j] = 9;
          }
        }	else if (i<26){
        	if (j<11){
          	grid[i][j] = 25;
          } else if (j<23){
          	grid[i][j] = 26;
          } else if (j<24){
          	grid[i][j] = 27;
          } else if (j<26){
          	grid[i][j] = 28;
          } else if (j<29){
          	grid[i][j] = 29;
          } else {
          	grid[i][j] = 30;
          } 
        } else if (i<33){
        	if (j<11){
          	grid[i][j] = 25;
          } else if (j<23){
          	grid[i][j] = 26;
          } else if (j<24){
          	grid[i][j] = 31;
          } else if (j<26){
          	grid[i][j] = 32;
          } else if (j<29){
          	grid[i][j] = 33;
          } else{
          	grid[i][j] = 34;
          } 
        }	else {
        	if (j<11){
          	grid[i][j] = 25;
          } else if (j<23){
          	grid[i][j] = 35;
          } else if (j<24){
          	grid[i][j] = 36;
          } else if (j<29){
          	grid[i][j] = 37;
          } else{
          	grid[i][j] = 38;
          }
        }
        
      }
    }
    
    /* randomize
    let count = 1;
    for (i = 0; i < blockN; i++) {
      grid[i] = [];

      for (j = 0; j < blockN; j++) {
        grid[i][j] = count;
        count++;
      }
    }*/
}

function addCubes() {
		for(var i = 0; i < blockN; i++){
        for(var j = 0; j < blockN; j++){
        		z = THREE.MathUtils.randInt(1,blockZMultiple)*blockMinZ;
        		geometry = new THREE.BoxGeometry(blockMinSize,blockMinSize,z);
            /*geometry.deleteAttribute('normal');
            geometry.deleteAttribute('uv');
            geometry = mergeVertices(geometry);
            geometry.computeVertexNormals();*/
            setColor();
            material = new THREE.MeshLambertMaterial({color:color});
            mesh  = new THREE.Mesh(geometry, material);
        		mesh.position.x = - (blockN-1)/2*(blockMinSize + blockDistance) + ((blockMinSize + blockDistance) * i);
            mesh.position.y = - (blockN-1)/2*(blockMinSize + blockDistance) + ((blockMinSize + blockDistance) * j);
            mesh.position.z = -z/2;
            scene.add(mesh);
        }
    }
}

function setColor() {
	const colors = ['red','blue','yellow'];
  const randNum = THREE.MathUtils.randFloat(0,1);
  if (randNum <= 0.8) {
  	color = 'white';
  } else {
  	color = colors[THREE.MathUtils.randInt(0, 2)]
  }
}

function init() {
    // Renderer.
    renderer = new THREE.WebGLRenderer();
    //renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    // Add renderer to page
    document.body.appendChild(renderer.domElement);

    // Create camera.
    camera = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight,1, 10000);
    camera.position.z = 800;
    
    // Add controls
    controls = new OrbitControls(camera, renderer.domElement);
    controls.addEventListener('change',render);

    // Create scene.
    scene = new THREE.Scene();
    
    light1 = new THREE.HemisphereLight( 0xffffff, 0x000088 );
    light1.position.set( - 1, 1.5, 1 );
    scene.add( light1 );
    
    

    // Add listener for window resize.
    window.addEventListener('resize', onWindowResize, false);
    document.addEventListener('mousemove',onMouseMove);
}

function animate() {
    controls.update();
    renderer.render( scene, camera );
    requestAnimationFrame(animate);
}

function render() {
		/*
		theta += 0.1;

    camera.position.x = radius * Math.sin( THREE.MathUtils.degToRad( theta ) );
    camera.position.y = radius * Math.sin( THREE.MathUtils.degToRad( theta ) );
    camera.position.z = radius * Math.cos( THREE.MathUtils.degToRad( theta ) );
    camera.lookAt( scene.position );

    camera.updateMatrixWorld();*/
    
    const intersection = raycaster.intersectObject( mesh );
    if ( intersection.length > 0 ) {
      const instanceId = intersection[ 0 ].instanceId;
      mesh.setColorAt( instanceId, color.setHex( Math.random() * 0xffffff ) );
      mesh.instanceColor.needsUpdate = true;
    }
		renderer.render(scene, camera);
    
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
    controls.update();
}

function onMouseMove(event) {
		event.preventDefault();

    mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
    mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
}